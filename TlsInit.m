//
//  TlsInit.m
//  GnuTLSExt
//
//  Created by Dan on 17.09.2021.
//

#import "TlsInit.h"

@implementation TlsInit

+ (void)initialize {
    (void)NseInit.class;
    
    tls_init();
}

@end
